<?php
/**
 * Created by PhpStorm.
 * User: washington
 * Date: 02/03/15
 * Time: 23:22
 */

namespace Fcl\Dojo;


class StringHelper
{
    public static function removeSpecialChars($string)
    {
        return preg_replace('/[^A-Za-zÀ-ú\-]/', ' ', $string);
    }

    public static function separateWords($string)
    {
        return explode(' ', $string);
    }
} 