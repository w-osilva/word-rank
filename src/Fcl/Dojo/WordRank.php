<?php
/**
 * Created by PhpStorm.
 * User: washington
 * Date: 02/03/15
 * Time: 23:52
 */

namespace Fcl\Dojo;


class WordRank
{
    protected $words;

    public function __construct($string)
    {
        $this->words = StringHelper::separateWords($string);
    }

    public function getRankingWords()
    {
        return $this->sortListOfWords($this->countOcurrencesOfWords());
    }


    protected function sortListOfWords(array $list)
    {
        //sort keys
        ksort($list);

        //sort values
        $ordered = [];

        for($i = max($list); $i >= 0; $i--) {
            foreach($list as $key => $value){
                if($value == $i && !isset($ordered[$key])){
                    $ordered[$key] = $value;
                }
            }
        }
        return $ordered;
    }


    protected function countOcurrencesOfWords()
    {
        $list = [];
        foreach ($this->words as $word) {
            if($word){
                $index = mb_strtolower($word, "UTF-8");
                isset($list[$index]) ? $list[$index] +=1 : $list[$index] = 1;
            }
        }
        return $list;
    }

} 