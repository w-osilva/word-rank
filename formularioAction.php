<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', false);
require_once(realpath(__DIR__) . "/vendor/autoload.php");
use Fcl\Dojo\WordRank;

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $text = \Fcl\Dojo\StringHelper::removeSpecialChars($_POST['texto']);
    $wordRanking = new WordRank($text);
    $words = $wordRanking->getRankingWords();
}
?>

<html>
<head>
<meta charset="utf-8"/>
</head>
<body>
<?php
if(isset($words)) {
    echo "<pre>";
    print_r($words);
} else {
    echo "<a href='formulario.php'>Voltar ao formulario</a>";
}
?>
</body>
</html>






